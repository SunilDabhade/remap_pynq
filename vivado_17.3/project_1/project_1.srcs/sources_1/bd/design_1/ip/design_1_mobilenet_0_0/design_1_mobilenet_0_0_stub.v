// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.3 (lin64) Build 2018833 Wed Oct  4 19:58:07 MDT 2017
// Date        : Mon Aug 20 10:09:49 2018
// Host        : di running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/sunil/vivado_17.3/project_1/project_1.srcs/sources_1/bd/design_1/ip/design_1_mobilenet_0_0/design_1_mobilenet_0_0_stub.v
// Design      : design_1_mobilenet_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "mobilenet,Vivado 2017.3" *)
module design_1_mobilenet_0_0(s_axi_AXILiteS_AWADDR, 
  s_axi_AXILiteS_AWVALID, s_axi_AXILiteS_AWREADY, s_axi_AXILiteS_WDATA, 
  s_axi_AXILiteS_WSTRB, s_axi_AXILiteS_WVALID, s_axi_AXILiteS_WREADY, 
  s_axi_AXILiteS_BRESP, s_axi_AXILiteS_BVALID, s_axi_AXILiteS_BREADY, 
  s_axi_AXILiteS_ARADDR, s_axi_AXILiteS_ARVALID, s_axi_AXILiteS_ARREADY, 
  s_axi_AXILiteS_RDATA, s_axi_AXILiteS_RRESP, s_axi_AXILiteS_RVALID, 
  s_axi_AXILiteS_RREADY, ap_clk, ap_rst_n, interrupt, m_axi_IMG_AWADDR, m_axi_IMG_AWLEN, 
  m_axi_IMG_AWSIZE, m_axi_IMG_AWBURST, m_axi_IMG_AWLOCK, m_axi_IMG_AWREGION, 
  m_axi_IMG_AWCACHE, m_axi_IMG_AWPROT, m_axi_IMG_AWQOS, m_axi_IMG_AWVALID, 
  m_axi_IMG_AWREADY, m_axi_IMG_WDATA, m_axi_IMG_WSTRB, m_axi_IMG_WLAST, m_axi_IMG_WVALID, 
  m_axi_IMG_WREADY, m_axi_IMG_BRESP, m_axi_IMG_BVALID, m_axi_IMG_BREADY, m_axi_IMG_ARADDR, 
  m_axi_IMG_ARLEN, m_axi_IMG_ARSIZE, m_axi_IMG_ARBURST, m_axi_IMG_ARLOCK, 
  m_axi_IMG_ARREGION, m_axi_IMG_ARCACHE, m_axi_IMG_ARPROT, m_axi_IMG_ARQOS, 
  m_axi_IMG_ARVALID, m_axi_IMG_ARREADY, m_axi_IMG_RDATA, m_axi_IMG_RRESP, m_axi_IMG_RLAST, 
  m_axi_IMG_RVALID, m_axi_IMG_RREADY, m_axi_INPUT_r_AWADDR, m_axi_INPUT_r_AWLEN, 
  m_axi_INPUT_r_AWSIZE, m_axi_INPUT_r_AWBURST, m_axi_INPUT_r_AWLOCK, 
  m_axi_INPUT_r_AWREGION, m_axi_INPUT_r_AWCACHE, m_axi_INPUT_r_AWPROT, 
  m_axi_INPUT_r_AWQOS, m_axi_INPUT_r_AWVALID, m_axi_INPUT_r_AWREADY, m_axi_INPUT_r_WDATA, 
  m_axi_INPUT_r_WSTRB, m_axi_INPUT_r_WLAST, m_axi_INPUT_r_WVALID, m_axi_INPUT_r_WREADY, 
  m_axi_INPUT_r_BRESP, m_axi_INPUT_r_BVALID, m_axi_INPUT_r_BREADY, m_axi_INPUT_r_ARADDR, 
  m_axi_INPUT_r_ARLEN, m_axi_INPUT_r_ARSIZE, m_axi_INPUT_r_ARBURST, m_axi_INPUT_r_ARLOCK, 
  m_axi_INPUT_r_ARREGION, m_axi_INPUT_r_ARCACHE, m_axi_INPUT_r_ARPROT, 
  m_axi_INPUT_r_ARQOS, m_axi_INPUT_r_ARVALID, m_axi_INPUT_r_ARREADY, m_axi_INPUT_r_RDATA, 
  m_axi_INPUT_r_RRESP, m_axi_INPUT_r_RLAST, m_axi_INPUT_r_RVALID, m_axi_INPUT_r_RREADY, 
  m_axi_OUTPUT_r_AWADDR, m_axi_OUTPUT_r_AWLEN, m_axi_OUTPUT_r_AWSIZE, 
  m_axi_OUTPUT_r_AWBURST, m_axi_OUTPUT_r_AWLOCK, m_axi_OUTPUT_r_AWREGION, 
  m_axi_OUTPUT_r_AWCACHE, m_axi_OUTPUT_r_AWPROT, m_axi_OUTPUT_r_AWQOS, 
  m_axi_OUTPUT_r_AWVALID, m_axi_OUTPUT_r_AWREADY, m_axi_OUTPUT_r_WDATA, 
  m_axi_OUTPUT_r_WSTRB, m_axi_OUTPUT_r_WLAST, m_axi_OUTPUT_r_WVALID, 
  m_axi_OUTPUT_r_WREADY, m_axi_OUTPUT_r_BRESP, m_axi_OUTPUT_r_BVALID, 
  m_axi_OUTPUT_r_BREADY, m_axi_OUTPUT_r_ARADDR, m_axi_OUTPUT_r_ARLEN, 
  m_axi_OUTPUT_r_ARSIZE, m_axi_OUTPUT_r_ARBURST, m_axi_OUTPUT_r_ARLOCK, 
  m_axi_OUTPUT_r_ARREGION, m_axi_OUTPUT_r_ARCACHE, m_axi_OUTPUT_r_ARPROT, 
  m_axi_OUTPUT_r_ARQOS, m_axi_OUTPUT_r_ARVALID, m_axi_OUTPUT_r_ARREADY, 
  m_axi_OUTPUT_r_RDATA, m_axi_OUTPUT_r_RRESP, m_axi_OUTPUT_r_RLAST, 
  m_axi_OUTPUT_r_RVALID, m_axi_OUTPUT_r_RREADY)
/* synthesis syn_black_box black_box_pad_pin="s_axi_AXILiteS_AWADDR[6:0],s_axi_AXILiteS_AWVALID,s_axi_AXILiteS_AWREADY,s_axi_AXILiteS_WDATA[31:0],s_axi_AXILiteS_WSTRB[3:0],s_axi_AXILiteS_WVALID,s_axi_AXILiteS_WREADY,s_axi_AXILiteS_BRESP[1:0],s_axi_AXILiteS_BVALID,s_axi_AXILiteS_BREADY,s_axi_AXILiteS_ARADDR[6:0],s_axi_AXILiteS_ARVALID,s_axi_AXILiteS_ARREADY,s_axi_AXILiteS_RDATA[31:0],s_axi_AXILiteS_RRESP[1:0],s_axi_AXILiteS_RVALID,s_axi_AXILiteS_RREADY,ap_clk,ap_rst_n,interrupt,m_axi_IMG_AWADDR[31:0],m_axi_IMG_AWLEN[7:0],m_axi_IMG_AWSIZE[2:0],m_axi_IMG_AWBURST[1:0],m_axi_IMG_AWLOCK[1:0],m_axi_IMG_AWREGION[3:0],m_axi_IMG_AWCACHE[3:0],m_axi_IMG_AWPROT[2:0],m_axi_IMG_AWQOS[3:0],m_axi_IMG_AWVALID,m_axi_IMG_AWREADY,m_axi_IMG_WDATA[31:0],m_axi_IMG_WSTRB[3:0],m_axi_IMG_WLAST,m_axi_IMG_WVALID,m_axi_IMG_WREADY,m_axi_IMG_BRESP[1:0],m_axi_IMG_BVALID,m_axi_IMG_BREADY,m_axi_IMG_ARADDR[31:0],m_axi_IMG_ARLEN[7:0],m_axi_IMG_ARSIZE[2:0],m_axi_IMG_ARBURST[1:0],m_axi_IMG_ARLOCK[1:0],m_axi_IMG_ARREGION[3:0],m_axi_IMG_ARCACHE[3:0],m_axi_IMG_ARPROT[2:0],m_axi_IMG_ARQOS[3:0],m_axi_IMG_ARVALID,m_axi_IMG_ARREADY,m_axi_IMG_RDATA[31:0],m_axi_IMG_RRESP[1:0],m_axi_IMG_RLAST,m_axi_IMG_RVALID,m_axi_IMG_RREADY,m_axi_INPUT_r_AWADDR[31:0],m_axi_INPUT_r_AWLEN[7:0],m_axi_INPUT_r_AWSIZE[2:0],m_axi_INPUT_r_AWBURST[1:0],m_axi_INPUT_r_AWLOCK[1:0],m_axi_INPUT_r_AWREGION[3:0],m_axi_INPUT_r_AWCACHE[3:0],m_axi_INPUT_r_AWPROT[2:0],m_axi_INPUT_r_AWQOS[3:0],m_axi_INPUT_r_AWVALID,m_axi_INPUT_r_AWREADY,m_axi_INPUT_r_WDATA[31:0],m_axi_INPUT_r_WSTRB[3:0],m_axi_INPUT_r_WLAST,m_axi_INPUT_r_WVALID,m_axi_INPUT_r_WREADY,m_axi_INPUT_r_BRESP[1:0],m_axi_INPUT_r_BVALID,m_axi_INPUT_r_BREADY,m_axi_INPUT_r_ARADDR[31:0],m_axi_INPUT_r_ARLEN[7:0],m_axi_INPUT_r_ARSIZE[2:0],m_axi_INPUT_r_ARBURST[1:0],m_axi_INPUT_r_ARLOCK[1:0],m_axi_INPUT_r_ARREGION[3:0],m_axi_INPUT_r_ARCACHE[3:0],m_axi_INPUT_r_ARPROT[2:0],m_axi_INPUT_r_ARQOS[3:0],m_axi_INPUT_r_ARVALID,m_axi_INPUT_r_ARREADY,m_axi_INPUT_r_RDATA[31:0],m_axi_INPUT_r_RRESP[1:0],m_axi_INPUT_r_RLAST,m_axi_INPUT_r_RVALID,m_axi_INPUT_r_RREADY,m_axi_OUTPUT_r_AWADDR[31:0],m_axi_OUTPUT_r_AWLEN[7:0],m_axi_OUTPUT_r_AWSIZE[2:0],m_axi_OUTPUT_r_AWBURST[1:0],m_axi_OUTPUT_r_AWLOCK[1:0],m_axi_OUTPUT_r_AWREGION[3:0],m_axi_OUTPUT_r_AWCACHE[3:0],m_axi_OUTPUT_r_AWPROT[2:0],m_axi_OUTPUT_r_AWQOS[3:0],m_axi_OUTPUT_r_AWVALID,m_axi_OUTPUT_r_AWREADY,m_axi_OUTPUT_r_WDATA[31:0],m_axi_OUTPUT_r_WSTRB[3:0],m_axi_OUTPUT_r_WLAST,m_axi_OUTPUT_r_WVALID,m_axi_OUTPUT_r_WREADY,m_axi_OUTPUT_r_BRESP[1:0],m_axi_OUTPUT_r_BVALID,m_axi_OUTPUT_r_BREADY,m_axi_OUTPUT_r_ARADDR[31:0],m_axi_OUTPUT_r_ARLEN[7:0],m_axi_OUTPUT_r_ARSIZE[2:0],m_axi_OUTPUT_r_ARBURST[1:0],m_axi_OUTPUT_r_ARLOCK[1:0],m_axi_OUTPUT_r_ARREGION[3:0],m_axi_OUTPUT_r_ARCACHE[3:0],m_axi_OUTPUT_r_ARPROT[2:0],m_axi_OUTPUT_r_ARQOS[3:0],m_axi_OUTPUT_r_ARVALID,m_axi_OUTPUT_r_ARREADY,m_axi_OUTPUT_r_RDATA[31:0],m_axi_OUTPUT_r_RRESP[1:0],m_axi_OUTPUT_r_RLAST,m_axi_OUTPUT_r_RVALID,m_axi_OUTPUT_r_RREADY" */;
  input [6:0]s_axi_AXILiteS_AWADDR;
  input s_axi_AXILiteS_AWVALID;
  output s_axi_AXILiteS_AWREADY;
  input [31:0]s_axi_AXILiteS_WDATA;
  input [3:0]s_axi_AXILiteS_WSTRB;
  input s_axi_AXILiteS_WVALID;
  output s_axi_AXILiteS_WREADY;
  output [1:0]s_axi_AXILiteS_BRESP;
  output s_axi_AXILiteS_BVALID;
  input s_axi_AXILiteS_BREADY;
  input [6:0]s_axi_AXILiteS_ARADDR;
  input s_axi_AXILiteS_ARVALID;
  output s_axi_AXILiteS_ARREADY;
  output [31:0]s_axi_AXILiteS_RDATA;
  output [1:0]s_axi_AXILiteS_RRESP;
  output s_axi_AXILiteS_RVALID;
  input s_axi_AXILiteS_RREADY;
  input ap_clk;
  input ap_rst_n;
  output interrupt;
  output [31:0]m_axi_IMG_AWADDR;
  output [7:0]m_axi_IMG_AWLEN;
  output [2:0]m_axi_IMG_AWSIZE;
  output [1:0]m_axi_IMG_AWBURST;
  output [1:0]m_axi_IMG_AWLOCK;
  output [3:0]m_axi_IMG_AWREGION;
  output [3:0]m_axi_IMG_AWCACHE;
  output [2:0]m_axi_IMG_AWPROT;
  output [3:0]m_axi_IMG_AWQOS;
  output m_axi_IMG_AWVALID;
  input m_axi_IMG_AWREADY;
  output [31:0]m_axi_IMG_WDATA;
  output [3:0]m_axi_IMG_WSTRB;
  output m_axi_IMG_WLAST;
  output m_axi_IMG_WVALID;
  input m_axi_IMG_WREADY;
  input [1:0]m_axi_IMG_BRESP;
  input m_axi_IMG_BVALID;
  output m_axi_IMG_BREADY;
  output [31:0]m_axi_IMG_ARADDR;
  output [7:0]m_axi_IMG_ARLEN;
  output [2:0]m_axi_IMG_ARSIZE;
  output [1:0]m_axi_IMG_ARBURST;
  output [1:0]m_axi_IMG_ARLOCK;
  output [3:0]m_axi_IMG_ARREGION;
  output [3:0]m_axi_IMG_ARCACHE;
  output [2:0]m_axi_IMG_ARPROT;
  output [3:0]m_axi_IMG_ARQOS;
  output m_axi_IMG_ARVALID;
  input m_axi_IMG_ARREADY;
  input [31:0]m_axi_IMG_RDATA;
  input [1:0]m_axi_IMG_RRESP;
  input m_axi_IMG_RLAST;
  input m_axi_IMG_RVALID;
  output m_axi_IMG_RREADY;
  output [31:0]m_axi_INPUT_r_AWADDR;
  output [7:0]m_axi_INPUT_r_AWLEN;
  output [2:0]m_axi_INPUT_r_AWSIZE;
  output [1:0]m_axi_INPUT_r_AWBURST;
  output [1:0]m_axi_INPUT_r_AWLOCK;
  output [3:0]m_axi_INPUT_r_AWREGION;
  output [3:0]m_axi_INPUT_r_AWCACHE;
  output [2:0]m_axi_INPUT_r_AWPROT;
  output [3:0]m_axi_INPUT_r_AWQOS;
  output m_axi_INPUT_r_AWVALID;
  input m_axi_INPUT_r_AWREADY;
  output [31:0]m_axi_INPUT_r_WDATA;
  output [3:0]m_axi_INPUT_r_WSTRB;
  output m_axi_INPUT_r_WLAST;
  output m_axi_INPUT_r_WVALID;
  input m_axi_INPUT_r_WREADY;
  input [1:0]m_axi_INPUT_r_BRESP;
  input m_axi_INPUT_r_BVALID;
  output m_axi_INPUT_r_BREADY;
  output [31:0]m_axi_INPUT_r_ARADDR;
  output [7:0]m_axi_INPUT_r_ARLEN;
  output [2:0]m_axi_INPUT_r_ARSIZE;
  output [1:0]m_axi_INPUT_r_ARBURST;
  output [1:0]m_axi_INPUT_r_ARLOCK;
  output [3:0]m_axi_INPUT_r_ARREGION;
  output [3:0]m_axi_INPUT_r_ARCACHE;
  output [2:0]m_axi_INPUT_r_ARPROT;
  output [3:0]m_axi_INPUT_r_ARQOS;
  output m_axi_INPUT_r_ARVALID;
  input m_axi_INPUT_r_ARREADY;
  input [31:0]m_axi_INPUT_r_RDATA;
  input [1:0]m_axi_INPUT_r_RRESP;
  input m_axi_INPUT_r_RLAST;
  input m_axi_INPUT_r_RVALID;
  output m_axi_INPUT_r_RREADY;
  output [31:0]m_axi_OUTPUT_r_AWADDR;
  output [7:0]m_axi_OUTPUT_r_AWLEN;
  output [2:0]m_axi_OUTPUT_r_AWSIZE;
  output [1:0]m_axi_OUTPUT_r_AWBURST;
  output [1:0]m_axi_OUTPUT_r_AWLOCK;
  output [3:0]m_axi_OUTPUT_r_AWREGION;
  output [3:0]m_axi_OUTPUT_r_AWCACHE;
  output [2:0]m_axi_OUTPUT_r_AWPROT;
  output [3:0]m_axi_OUTPUT_r_AWQOS;
  output m_axi_OUTPUT_r_AWVALID;
  input m_axi_OUTPUT_r_AWREADY;
  output [31:0]m_axi_OUTPUT_r_WDATA;
  output [3:0]m_axi_OUTPUT_r_WSTRB;
  output m_axi_OUTPUT_r_WLAST;
  output m_axi_OUTPUT_r_WVALID;
  input m_axi_OUTPUT_r_WREADY;
  input [1:0]m_axi_OUTPUT_r_BRESP;
  input m_axi_OUTPUT_r_BVALID;
  output m_axi_OUTPUT_r_BREADY;
  output [31:0]m_axi_OUTPUT_r_ARADDR;
  output [7:0]m_axi_OUTPUT_r_ARLEN;
  output [2:0]m_axi_OUTPUT_r_ARSIZE;
  output [1:0]m_axi_OUTPUT_r_ARBURST;
  output [1:0]m_axi_OUTPUT_r_ARLOCK;
  output [3:0]m_axi_OUTPUT_r_ARREGION;
  output [3:0]m_axi_OUTPUT_r_ARCACHE;
  output [2:0]m_axi_OUTPUT_r_ARPROT;
  output [3:0]m_axi_OUTPUT_r_ARQOS;
  output m_axi_OUTPUT_r_ARVALID;
  input m_axi_OUTPUT_r_ARREADY;
  input [31:0]m_axi_OUTPUT_r_RDATA;
  input [1:0]m_axi_OUTPUT_r_RRESP;
  input m_axi_OUTPUT_r_RLAST;
  input m_axi_OUTPUT_r_RVALID;
  output m_axi_OUTPUT_r_RREADY;
endmodule
