
`timescale 1 ns / 1 ps

  module mobilenet_mul_multde_DSP48_14(a, b, p);
input [18 - 1 : 0] a; // synthesis attribute keep a "true"
input [18 - 1 : 0] b; // synthesis attribute keep b "true"
output [36 - 1 : 0] p; // synthesis attribute keep p "true"

assign p = $unsigned (a) * $unsigned (b);

endmodule

`timescale 1 ns / 1 ps
module mobilenet_mul_multde(
    din0,
    din1,
    dout);

parameter ID = 32'd1;
parameter NUM_STAGE = 32'd1;
parameter din0_WIDTH = 32'd1;
parameter din1_WIDTH = 32'd1;
parameter dout_WIDTH = 32'd1;
input[din0_WIDTH - 1:0] din0;
input[din1_WIDTH - 1:0] din1;
output[dout_WIDTH - 1:0] dout;



mobilenet_mul_multde_DSP48_14 mobilenet_mul_multde_DSP48_14_U(
    .a( din0 ),
    .b( din1 ),
    .p( dout ));

endmodule

