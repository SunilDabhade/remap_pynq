// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2017.3
// Copyright (C) 1986-2017 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module load_weight_2D_from_s (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        dest_V_address0,
        dest_V_ce0,
        dest_V_we0,
        dest_V_d0,
        m_axi_src_V_AWVALID,
        m_axi_src_V_AWREADY,
        m_axi_src_V_AWADDR,
        m_axi_src_V_AWID,
        m_axi_src_V_AWLEN,
        m_axi_src_V_AWSIZE,
        m_axi_src_V_AWBURST,
        m_axi_src_V_AWLOCK,
        m_axi_src_V_AWCACHE,
        m_axi_src_V_AWPROT,
        m_axi_src_V_AWQOS,
        m_axi_src_V_AWREGION,
        m_axi_src_V_AWUSER,
        m_axi_src_V_WVALID,
        m_axi_src_V_WREADY,
        m_axi_src_V_WDATA,
        m_axi_src_V_WSTRB,
        m_axi_src_V_WLAST,
        m_axi_src_V_WID,
        m_axi_src_V_WUSER,
        m_axi_src_V_ARVALID,
        m_axi_src_V_ARREADY,
        m_axi_src_V_ARADDR,
        m_axi_src_V_ARID,
        m_axi_src_V_ARLEN,
        m_axi_src_V_ARSIZE,
        m_axi_src_V_ARBURST,
        m_axi_src_V_ARLOCK,
        m_axi_src_V_ARCACHE,
        m_axi_src_V_ARPROT,
        m_axi_src_V_ARQOS,
        m_axi_src_V_ARREGION,
        m_axi_src_V_ARUSER,
        m_axi_src_V_RVALID,
        m_axi_src_V_RREADY,
        m_axi_src_V_RDATA,
        m_axi_src_V_RLAST,
        m_axi_src_V_RID,
        m_axi_src_V_RUSER,
        m_axi_src_V_RRESP,
        m_axi_src_V_BVALID,
        m_axi_src_V_BREADY,
        m_axi_src_V_BRESP,
        m_axi_src_V_BID,
        m_axi_src_V_BUSER,
        src_V_offset,
        src_V_offset_offset
);

parameter    ap_ST_fsm_state1 = 3'd1;
parameter    ap_ST_fsm_pp0_stage0 = 3'd2;
parameter    ap_ST_fsm_state13 = 3'd4;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
output  [7:0] dest_V_address0;
output   dest_V_ce0;
output   dest_V_we0;
output  [7:0] dest_V_d0;
output   m_axi_src_V_AWVALID;
input   m_axi_src_V_AWREADY;
output  [31:0] m_axi_src_V_AWADDR;
output  [0:0] m_axi_src_V_AWID;
output  [31:0] m_axi_src_V_AWLEN;
output  [2:0] m_axi_src_V_AWSIZE;
output  [1:0] m_axi_src_V_AWBURST;
output  [1:0] m_axi_src_V_AWLOCK;
output  [3:0] m_axi_src_V_AWCACHE;
output  [2:0] m_axi_src_V_AWPROT;
output  [3:0] m_axi_src_V_AWQOS;
output  [3:0] m_axi_src_V_AWREGION;
output  [0:0] m_axi_src_V_AWUSER;
output   m_axi_src_V_WVALID;
input   m_axi_src_V_WREADY;
output  [15:0] m_axi_src_V_WDATA;
output  [1:0] m_axi_src_V_WSTRB;
output   m_axi_src_V_WLAST;
output  [0:0] m_axi_src_V_WID;
output  [0:0] m_axi_src_V_WUSER;
output   m_axi_src_V_ARVALID;
input   m_axi_src_V_ARREADY;
output  [31:0] m_axi_src_V_ARADDR;
output  [0:0] m_axi_src_V_ARID;
output  [31:0] m_axi_src_V_ARLEN;
output  [2:0] m_axi_src_V_ARSIZE;
output  [1:0] m_axi_src_V_ARBURST;
output  [1:0] m_axi_src_V_ARLOCK;
output  [3:0] m_axi_src_V_ARCACHE;
output  [2:0] m_axi_src_V_ARPROT;
output  [3:0] m_axi_src_V_ARQOS;
output  [3:0] m_axi_src_V_ARREGION;
output  [0:0] m_axi_src_V_ARUSER;
input   m_axi_src_V_RVALID;
output   m_axi_src_V_RREADY;
input  [15:0] m_axi_src_V_RDATA;
input   m_axi_src_V_RLAST;
input  [0:0] m_axi_src_V_RID;
input  [0:0] m_axi_src_V_RUSER;
input  [1:0] m_axi_src_V_RRESP;
input   m_axi_src_V_BVALID;
output   m_axi_src_V_BREADY;
input  [1:0] m_axi_src_V_BRESP;
input  [0:0] m_axi_src_V_BID;
input  [0:0] m_axi_src_V_BUSER;
input  [30:0] src_V_offset;
input  [9:0] src_V_offset_offset;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg dest_V_ce0;
reg dest_V_we0;
reg m_axi_src_V_ARVALID;
reg m_axi_src_V_RREADY;

(* fsm_encoding = "none" *) reg   [2:0] ap_CS_fsm;
wire    ap_CS_fsm_state1;
reg    src_V_blk_n_AR;
reg    ap_enable_reg_pp0_iter2;
wire    ap_block_pp0_stage0;
reg   [0:0] exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter1_exitcond_flatten_reg_399;
reg    src_V_blk_n_R;
reg    ap_enable_reg_pp0_iter9;
reg   [0:0] ap_reg_pp0_iter8_exitcond_flatten_reg_399;
reg   [8:0] indvar_flatten_reg_130;
reg   [4:0] i_reg_141;
reg   [4:0] j_reg_152;
wire   [14:0] tmp_78_cast_fu_171_p1;
reg   [14:0] tmp_78_cast_reg_389;
wire   [63:0] sext_fu_175_p1;
reg   [63:0] sext_reg_394;
wire   [0:0] exitcond_flatten_fu_179_p2;
wire    ap_CS_fsm_pp0_stage0;
wire    ap_block_state2_pp0_stage0_iter0;
wire    ap_block_state3_pp0_stage0_iter1;
wire    ap_block_state4_pp0_stage0_iter2;
reg    ap_sig_ioackin_m_axi_src_V_ARREADY;
reg    ap_block_state4_io;
wire    ap_block_state5_pp0_stage0_iter3;
wire    ap_block_state6_pp0_stage0_iter4;
wire    ap_block_state7_pp0_stage0_iter5;
wire    ap_block_state8_pp0_stage0_iter6;
wire    ap_block_state9_pp0_stage0_iter7;
wire    ap_block_state10_pp0_stage0_iter8;
reg    ap_block_state11_pp0_stage0_iter9;
wire    ap_block_state12_pp0_stage0_iter10;
reg    ap_block_pp0_stage0_11001;
reg   [0:0] ap_reg_pp0_iter2_exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter3_exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter4_exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter5_exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter6_exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter7_exitcond_flatten_reg_399;
reg   [0:0] ap_reg_pp0_iter9_exitcond_flatten_reg_399;
wire   [8:0] indvar_flatten_next_fu_185_p2;
reg    ap_enable_reg_pp0_iter0;
wire   [4:0] j_mid2_fu_203_p3;
reg   [4:0] j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter1_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter2_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter3_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter4_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter5_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter6_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter7_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter8_j_mid2_reg_408;
reg   [4:0] ap_reg_pp0_iter9_j_mid2_reg_408;
wire   [4:0] tmp_mid2_v_fu_211_p3;
reg   [4:0] tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter1_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter2_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter3_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter4_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter5_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter6_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter7_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter8_tmp_mid2_v_reg_414;
reg   [4:0] ap_reg_pp0_iter9_tmp_mid2_v_reg_414;
wire   [4:0] j_4_fu_219_p2;
reg   [31:0] src_V_addr_reg_426;
reg   [15:0] p_Val2_s_reg_432;
reg   [0:0] signbit_reg_440;
reg    ap_block_pp0_stage0_subdone;
reg    ap_condition_pp0_exit_iter0_state2;
reg    ap_enable_reg_pp0_iter1;
reg    ap_enable_reg_pp0_iter3;
reg    ap_enable_reg_pp0_iter4;
reg    ap_enable_reg_pp0_iter5;
reg    ap_enable_reg_pp0_iter6;
reg    ap_enable_reg_pp0_iter7;
reg    ap_enable_reg_pp0_iter8;
reg    ap_enable_reg_pp0_iter10;
reg   [4:0] ap_phi_mux_i_phi_fu_145_p4;
wire   [63:0] tmp_84_cast_fu_293_p1;
wire   [63:0] sum_fu_254_p2;
reg    ap_reg_ioackin_m_axi_src_V_ARREADY;
reg    ap_block_pp0_stage0_01001;
wire   [13:0] tmp_fu_163_p3;
wire   [0:0] exitcond7_fu_197_p2;
wire   [4:0] i_3_fu_191_p2;
wire   [14:0] tmp_mid2_cast_fu_225_p1;
wire   [14:0] tmp_70_fu_228_p2;
wire   [18:0] tmp_104_fu_233_p3;
wire   [63:0] tmp_71_fu_241_p1;
wire   [63:0] tmp_s_fu_245_p1;
wire   [63:0] tmp_73_fu_248_p2;
wire   [8:0] tmp_69_fu_273_p3;
wire   [9:0] tmp_80_cast_fu_280_p1;
wire   [9:0] tmp_cast_fu_284_p1;
wire   [9:0] tmp_72_fu_287_p2;
wire   [0:0] tmp_107_fu_314_p1;
wire   [0:0] tmp_106_fu_307_p3;
wire   [5:0] tmp_52_fu_323_p4;
wire   [0:0] tmp_51_fu_317_p2;
wire   [6:0] tmp_53_fu_332_p3;
wire   [0:0] tmp_54_fu_340_p2;
wire   [0:0] qb_assign_fu_346_p2;
wire   [7:0] tmp_55_fu_351_p1;
wire   [7:0] p_Val2_9_fu_298_p4;
wire   [7:0] p_Val2_10_fu_355_p2;
wire   [0:0] tmp_108_fu_361_p3;
wire   [0:0] tmp_56_fu_369_p2;
wire   [0:0] overflow_fu_374_p2;
wire    ap_CS_fsm_state13;
reg   [2:0] ap_NS_fsm;
reg    ap_idle_pp0;
wire    ap_enable_pp0;

// power-on initialization
initial begin
#0 ap_CS_fsm = 3'd1;
#0 ap_enable_reg_pp0_iter2 = 1'b0;
#0 ap_enable_reg_pp0_iter9 = 1'b0;
#0 ap_enable_reg_pp0_iter0 = 1'b0;
#0 ap_enable_reg_pp0_iter1 = 1'b0;
#0 ap_enable_reg_pp0_iter3 = 1'b0;
#0 ap_enable_reg_pp0_iter4 = 1'b0;
#0 ap_enable_reg_pp0_iter5 = 1'b0;
#0 ap_enable_reg_pp0_iter6 = 1'b0;
#0 ap_enable_reg_pp0_iter7 = 1'b0;
#0 ap_enable_reg_pp0_iter8 = 1'b0;
#0 ap_enable_reg_pp0_iter10 = 1'b0;
#0 ap_reg_ioackin_m_axi_src_V_ARREADY = 1'b0;
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_fsm_state1;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter0 <= 1'b0;
    end else begin
        if (((ap_block_pp0_stage0_subdone == 1'b0) & (1'b1 == ap_condition_pp0_exit_iter0_state2) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
            ap_enable_reg_pp0_iter0 <= 1'b0;
        end else if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
            ap_enable_reg_pp0_iter0 <= 1'b1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter1 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            if ((1'b1 == ap_condition_pp0_exit_iter0_state2)) begin
                ap_enable_reg_pp0_iter1 <= (ap_condition_pp0_exit_iter0_state2 ^ 1'b1);
            end else if ((1'b1 == 1'b1)) begin
                ap_enable_reg_pp0_iter1 <= ap_enable_reg_pp0_iter0;
            end
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter10 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter10 <= ap_enable_reg_pp0_iter9;
        end else if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
            ap_enable_reg_pp0_iter10 <= 1'b0;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter2 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter2 <= ap_enable_reg_pp0_iter1;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter3 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter3 <= ap_enable_reg_pp0_iter2;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter4 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter4 <= ap_enable_reg_pp0_iter3;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter5 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter5 <= ap_enable_reg_pp0_iter4;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter6 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter6 <= ap_enable_reg_pp0_iter5;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter7 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter7 <= ap_enable_reg_pp0_iter6;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter8 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter8 <= ap_enable_reg_pp0_iter7;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_enable_reg_pp0_iter9 <= 1'b0;
    end else begin
        if ((ap_block_pp0_stage0_subdone == 1'b0)) begin
            ap_enable_reg_pp0_iter9 <= ap_enable_reg_pp0_iter8;
        end
    end
end

always @ (posedge ap_clk) begin
    if (ap_rst == 1'b1) begin
        ap_reg_ioackin_m_axi_src_V_ARREADY <= 1'b0;
    end else begin
        if (((1'd0 == ap_reg_pp0_iter1_exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter2))) begin
            if ((ap_block_pp0_stage0_11001 == 1'b0)) begin
                ap_reg_ioackin_m_axi_src_V_ARREADY <= 1'b0;
            end else if (((ap_block_pp0_stage0_01001 == 1'b0) & (1'b1 == m_axi_src_V_ARREADY))) begin
                ap_reg_ioackin_m_axi_src_V_ARREADY <= 1'b1;
            end
        end
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter1) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        i_reg_141 <= tmp_mid2_v_reg_414;
    end else if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
        i_reg_141 <= 5'd0;
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == exitcond_flatten_fu_179_p2) & (1'b1 == ap_enable_reg_pp0_iter0) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        indvar_flatten_reg_130 <= indvar_flatten_next_fu_185_p2;
    end else if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
        indvar_flatten_reg_130 <= 9'd0;
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == exitcond_flatten_fu_179_p2) & (1'b1 == ap_enable_reg_pp0_iter0) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        j_reg_152 <= j_4_fu_219_p2;
    end else if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
        j_reg_152 <= 5'd0;
    end
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        ap_reg_pp0_iter1_exitcond_flatten_reg_399 <= exitcond_flatten_reg_399;
        ap_reg_pp0_iter1_j_mid2_reg_408 <= j_mid2_reg_408;
        ap_reg_pp0_iter1_tmp_mid2_v_reg_414 <= tmp_mid2_v_reg_414;
        exitcond_flatten_reg_399 <= exitcond_flatten_fu_179_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_block_pp0_stage0_11001 == 1'b0)) begin
        ap_reg_pp0_iter2_exitcond_flatten_reg_399 <= ap_reg_pp0_iter1_exitcond_flatten_reg_399;
        ap_reg_pp0_iter2_j_mid2_reg_408 <= ap_reg_pp0_iter1_j_mid2_reg_408;
        ap_reg_pp0_iter2_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter1_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter3_exitcond_flatten_reg_399 <= ap_reg_pp0_iter2_exitcond_flatten_reg_399;
        ap_reg_pp0_iter3_j_mid2_reg_408 <= ap_reg_pp0_iter2_j_mid2_reg_408;
        ap_reg_pp0_iter3_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter2_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter4_exitcond_flatten_reg_399 <= ap_reg_pp0_iter3_exitcond_flatten_reg_399;
        ap_reg_pp0_iter4_j_mid2_reg_408 <= ap_reg_pp0_iter3_j_mid2_reg_408;
        ap_reg_pp0_iter4_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter3_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter5_exitcond_flatten_reg_399 <= ap_reg_pp0_iter4_exitcond_flatten_reg_399;
        ap_reg_pp0_iter5_j_mid2_reg_408 <= ap_reg_pp0_iter4_j_mid2_reg_408;
        ap_reg_pp0_iter5_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter4_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter6_exitcond_flatten_reg_399 <= ap_reg_pp0_iter5_exitcond_flatten_reg_399;
        ap_reg_pp0_iter6_j_mid2_reg_408 <= ap_reg_pp0_iter5_j_mid2_reg_408;
        ap_reg_pp0_iter6_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter5_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter7_exitcond_flatten_reg_399 <= ap_reg_pp0_iter6_exitcond_flatten_reg_399;
        ap_reg_pp0_iter7_j_mid2_reg_408 <= ap_reg_pp0_iter6_j_mid2_reg_408;
        ap_reg_pp0_iter7_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter6_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter8_exitcond_flatten_reg_399 <= ap_reg_pp0_iter7_exitcond_flatten_reg_399;
        ap_reg_pp0_iter8_j_mid2_reg_408 <= ap_reg_pp0_iter7_j_mid2_reg_408;
        ap_reg_pp0_iter8_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter7_tmp_mid2_v_reg_414;
        ap_reg_pp0_iter9_exitcond_flatten_reg_399 <= ap_reg_pp0_iter8_exitcond_flatten_reg_399;
        ap_reg_pp0_iter9_j_mid2_reg_408 <= ap_reg_pp0_iter8_j_mid2_reg_408;
        ap_reg_pp0_iter9_tmp_mid2_v_reg_414 <= ap_reg_pp0_iter8_tmp_mid2_v_reg_414;
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == exitcond_flatten_fu_179_p2) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        j_mid2_reg_408 <= j_mid2_fu_203_p3;
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        p_Val2_s_reg_432 <= m_axi_src_V_RDATA;
        signbit_reg_440 <= m_axi_src_V_RDATA[32'd15];
    end
end

always @ (posedge ap_clk) begin
    if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
        sext_reg_394[30 : 0] <= sext_fu_175_p1[30 : 0];
        tmp_78_cast_reg_389[13 : 4] <= tmp_78_cast_fu_171_p1[13 : 4];
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == exitcond_flatten_reg_399) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        src_V_addr_reg_426 <= sum_fu_254_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((1'd0 == exitcond_flatten_fu_179_p2) & (1'b1 == ap_enable_reg_pp0_iter0) & (1'b1 == ap_CS_fsm_pp0_stage0) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        tmp_mid2_v_reg_414 <= tmp_mid2_v_fu_211_p3;
    end
end

always @ (*) begin
    if ((exitcond_flatten_fu_179_p2 == 1'd1)) begin
        ap_condition_pp0_exit_iter0_state2 = 1'b1;
    end else begin
        ap_condition_pp0_exit_iter0_state2 = 1'b0;
    end
end

always @ (*) begin
    if (((1'b1 == ap_CS_fsm_state13) | ((1'b0 == ap_start) & (1'b1 == ap_CS_fsm_state1)))) begin
        ap_done = 1'b1;
    end else begin
        ap_done = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_start) & (1'b1 == ap_CS_fsm_state1))) begin
        ap_idle = 1'b1;
    end else begin
        ap_idle = 1'b0;
    end
end

always @ (*) begin
    if (((1'b0 == ap_enable_reg_pp0_iter2) & (1'b0 == ap_enable_reg_pp0_iter10) & (1'b0 == ap_enable_reg_pp0_iter8) & (1'b0 == ap_enable_reg_pp0_iter7) & (1'b0 == ap_enable_reg_pp0_iter6) & (1'b0 == ap_enable_reg_pp0_iter5) & (1'b0 == ap_enable_reg_pp0_iter4) & (1'b0 == ap_enable_reg_pp0_iter3) & (1'b0 == ap_enable_reg_pp0_iter1) & (1'b0 == ap_enable_reg_pp0_iter0) & (1'b0 == ap_enable_reg_pp0_iter9))) begin
        ap_idle_pp0 = 1'b1;
    end else begin
        ap_idle_pp0 = 1'b0;
    end
end

always @ (*) begin
    if (((ap_block_pp0_stage0 == 1'b0) & (1'd0 == exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter1) & (1'b1 == ap_CS_fsm_pp0_stage0))) begin
        ap_phi_mux_i_phi_fu_145_p4 = tmp_mid2_v_reg_414;
    end else begin
        ap_phi_mux_i_phi_fu_145_p4 = i_reg_141;
    end
end

always @ (*) begin
    if ((1'b1 == ap_CS_fsm_state13)) begin
        ap_ready = 1'b1;
    end else begin
        ap_ready = 1'b0;
    end
end

always @ (*) begin
    if ((1'b0 == ap_reg_ioackin_m_axi_src_V_ARREADY)) begin
        ap_sig_ioackin_m_axi_src_V_ARREADY = m_axi_src_V_ARREADY;
    end else begin
        ap_sig_ioackin_m_axi_src_V_ARREADY = 1'b1;
    end
end

always @ (*) begin
    if (((1'b1 == ap_enable_reg_pp0_iter10) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        dest_V_ce0 = 1'b1;
    end else begin
        dest_V_ce0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'd0 == ap_reg_pp0_iter9_exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter10) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        dest_V_we0 = 1'b1;
    end else begin
        dest_V_we0 = 1'b0;
    end
end

always @ (*) begin
    if (((1'd0 == ap_reg_pp0_iter1_exitcond_flatten_reg_399) & (1'b0 == ap_reg_ioackin_m_axi_src_V_ARREADY) & (ap_block_pp0_stage0_01001 == 1'b0) & (1'b1 == ap_enable_reg_pp0_iter2))) begin
        m_axi_src_V_ARVALID = 1'b1;
    end else begin
        m_axi_src_V_ARVALID = 1'b0;
    end
end

always @ (*) begin
    if (((1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter9) & (ap_block_pp0_stage0_11001 == 1'b0))) begin
        m_axi_src_V_RREADY = 1'b1;
    end else begin
        m_axi_src_V_RREADY = 1'b0;
    end
end

always @ (*) begin
    if (((ap_block_pp0_stage0 == 1'b0) & (1'd0 == ap_reg_pp0_iter1_exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter2))) begin
        src_V_blk_n_AR = m_axi_src_V_ARREADY;
    end else begin
        src_V_blk_n_AR = 1'b1;
    end
end

always @ (*) begin
    if (((ap_block_pp0_stage0 == 1'b0) & (1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (1'b1 == ap_enable_reg_pp0_iter9))) begin
        src_V_blk_n_R = m_axi_src_V_RVALID;
    end else begin
        src_V_blk_n_R = 1'b1;
    end
end

always @ (*) begin
    case (ap_CS_fsm)
        ap_ST_fsm_state1 : begin
            if (((ap_start == 1'b1) & (1'b1 == ap_CS_fsm_state1))) begin
                ap_NS_fsm = ap_ST_fsm_pp0_stage0;
            end else begin
                ap_NS_fsm = ap_ST_fsm_state1;
            end
        end
        ap_ST_fsm_pp0_stage0 : begin
            if ((~((ap_enable_reg_pp0_iter1 == 1'b0) & (ap_block_pp0_stage0_subdone == 1'b0) & (1'b1 == ap_enable_reg_pp0_iter0) & (exitcond_flatten_fu_179_p2 == 1'd1)) & ~((ap_block_pp0_stage0_subdone == 1'b0) & (1'b1 == ap_enable_reg_pp0_iter10) & (ap_enable_reg_pp0_iter9 == 1'b0)))) begin
                ap_NS_fsm = ap_ST_fsm_pp0_stage0;
            end else if ((((ap_block_pp0_stage0_subdone == 1'b0) & (1'b1 == ap_enable_reg_pp0_iter10) & (ap_enable_reg_pp0_iter9 == 1'b0)) | ((ap_enable_reg_pp0_iter1 == 1'b0) & (ap_block_pp0_stage0_subdone == 1'b0) & (1'b1 == ap_enable_reg_pp0_iter0) & (exitcond_flatten_fu_179_p2 == 1'd1)))) begin
                ap_NS_fsm = ap_ST_fsm_state13;
            end else begin
                ap_NS_fsm = ap_ST_fsm_pp0_stage0;
            end
        end
        ap_ST_fsm_state13 : begin
            ap_NS_fsm = ap_ST_fsm_state1;
        end
        default : begin
            ap_NS_fsm = 'bx;
        end
    endcase
end

assign ap_CS_fsm_pp0_stage0 = ap_CS_fsm[32'd1];

assign ap_CS_fsm_state1 = ap_CS_fsm[32'd0];

assign ap_CS_fsm_state13 = ap_CS_fsm[32'd2];

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_pp0_stage0_01001 = ((1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (1'b0 == m_axi_src_V_RVALID) & (1'b1 == ap_enable_reg_pp0_iter9));
end

always @ (*) begin
    ap_block_pp0_stage0_11001 = (((1'b1 == ap_enable_reg_pp0_iter2) & (1'b1 == ap_block_state4_io)) | ((1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (1'b0 == m_axi_src_V_RVALID) & (1'b1 == ap_enable_reg_pp0_iter9)));
end

always @ (*) begin
    ap_block_pp0_stage0_subdone = (((1'b1 == ap_enable_reg_pp0_iter2) & (1'b1 == ap_block_state4_io)) | ((1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (1'b0 == m_axi_src_V_RVALID) & (1'b1 == ap_enable_reg_pp0_iter9)));
end

assign ap_block_state10_pp0_stage0_iter8 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_state11_pp0_stage0_iter9 = ((1'd0 == ap_reg_pp0_iter8_exitcond_flatten_reg_399) & (1'b0 == m_axi_src_V_RVALID));
end

assign ap_block_state12_pp0_stage0_iter10 = ~(1'b1 == 1'b1);

assign ap_block_state2_pp0_stage0_iter0 = ~(1'b1 == 1'b1);

assign ap_block_state3_pp0_stage0_iter1 = ~(1'b1 == 1'b1);

always @ (*) begin
    ap_block_state4_io = ((1'd0 == ap_reg_pp0_iter1_exitcond_flatten_reg_399) & (1'b0 == ap_sig_ioackin_m_axi_src_V_ARREADY));
end

assign ap_block_state4_pp0_stage0_iter2 = ~(1'b1 == 1'b1);

assign ap_block_state5_pp0_stage0_iter3 = ~(1'b1 == 1'b1);

assign ap_block_state6_pp0_stage0_iter4 = ~(1'b1 == 1'b1);

assign ap_block_state7_pp0_stage0_iter5 = ~(1'b1 == 1'b1);

assign ap_block_state8_pp0_stage0_iter6 = ~(1'b1 == 1'b1);

assign ap_block_state9_pp0_stage0_iter7 = ~(1'b1 == 1'b1);

assign ap_enable_pp0 = (ap_idle_pp0 ^ 1'b1);

assign dest_V_address0 = tmp_84_cast_fu_293_p1;

assign dest_V_d0 = ((overflow_fu_374_p2[0:0] === 1'b1) ? 8'd127 : p_Val2_10_fu_355_p2);

assign exitcond7_fu_197_p2 = ((j_reg_152 == 5'd16) ? 1'b1 : 1'b0);

assign exitcond_flatten_fu_179_p2 = ((indvar_flatten_reg_130 == 9'd256) ? 1'b1 : 1'b0);

assign i_3_fu_191_p2 = (5'd1 + ap_phi_mux_i_phi_fu_145_p4);

assign indvar_flatten_next_fu_185_p2 = (indvar_flatten_reg_130 + 9'd1);

assign j_4_fu_219_p2 = (5'd1 + j_mid2_fu_203_p3);

assign j_mid2_fu_203_p3 = ((exitcond7_fu_197_p2[0:0] === 1'b1) ? 5'd0 : j_reg_152);

assign m_axi_src_V_ARADDR = src_V_addr_reg_426;

assign m_axi_src_V_ARBURST = 2'd0;

assign m_axi_src_V_ARCACHE = 4'd0;

assign m_axi_src_V_ARID = 1'd0;

assign m_axi_src_V_ARLEN = 32'd1;

assign m_axi_src_V_ARLOCK = 2'd0;

assign m_axi_src_V_ARPROT = 3'd0;

assign m_axi_src_V_ARQOS = 4'd0;

assign m_axi_src_V_ARREGION = 4'd0;

assign m_axi_src_V_ARSIZE = 3'd0;

assign m_axi_src_V_ARUSER = 1'd0;

assign m_axi_src_V_AWADDR = 32'd0;

assign m_axi_src_V_AWBURST = 2'd0;

assign m_axi_src_V_AWCACHE = 4'd0;

assign m_axi_src_V_AWID = 1'd0;

assign m_axi_src_V_AWLEN = 32'd0;

assign m_axi_src_V_AWLOCK = 2'd0;

assign m_axi_src_V_AWPROT = 3'd0;

assign m_axi_src_V_AWQOS = 4'd0;

assign m_axi_src_V_AWREGION = 4'd0;

assign m_axi_src_V_AWSIZE = 3'd0;

assign m_axi_src_V_AWUSER = 1'd0;

assign m_axi_src_V_AWVALID = 1'b0;

assign m_axi_src_V_BREADY = 1'b0;

assign m_axi_src_V_WDATA = 16'd0;

assign m_axi_src_V_WID = 1'd0;

assign m_axi_src_V_WLAST = 1'b0;

assign m_axi_src_V_WSTRB = 2'd0;

assign m_axi_src_V_WUSER = 1'd0;

assign m_axi_src_V_WVALID = 1'b0;

assign overflow_fu_374_p2 = (tmp_56_fu_369_p2 & tmp_108_fu_361_p3);

assign p_Val2_10_fu_355_p2 = (tmp_55_fu_351_p1 + p_Val2_9_fu_298_p4);

assign p_Val2_9_fu_298_p4 = {{p_Val2_s_reg_432[15:8]}};

assign qb_assign_fu_346_p2 = (tmp_54_fu_340_p2 & signbit_reg_440);

assign sext_fu_175_p1 = src_V_offset;

assign sum_fu_254_p2 = (sext_reg_394 + tmp_73_fu_248_p2);

assign tmp_104_fu_233_p3 = {{tmp_70_fu_228_p2}, {4'd0}};

assign tmp_106_fu_307_p3 = p_Val2_s_reg_432[32'd7];

assign tmp_107_fu_314_p1 = p_Val2_s_reg_432[0:0];

assign tmp_108_fu_361_p3 = p_Val2_10_fu_355_p2[32'd7];

assign tmp_51_fu_317_p2 = (tmp_107_fu_314_p1 | tmp_106_fu_307_p3);

assign tmp_52_fu_323_p4 = {{p_Val2_s_reg_432[6:1]}};

assign tmp_53_fu_332_p3 = {{tmp_52_fu_323_p4}, {tmp_51_fu_317_p2}};

assign tmp_54_fu_340_p2 = ((tmp_53_fu_332_p3 != 7'd0) ? 1'b1 : 1'b0);

assign tmp_55_fu_351_p1 = qb_assign_fu_346_p2;

assign tmp_56_fu_369_p2 = (signbit_reg_440 ^ 1'd1);

assign tmp_69_fu_273_p3 = {{ap_reg_pp0_iter9_tmp_mid2_v_reg_414}, {4'd0}};

assign tmp_70_fu_228_p2 = (tmp_78_cast_reg_389 + tmp_mid2_cast_fu_225_p1);

assign tmp_71_fu_241_p1 = tmp_104_fu_233_p3;

assign tmp_72_fu_287_p2 = (tmp_80_cast_fu_280_p1 + tmp_cast_fu_284_p1);

assign tmp_73_fu_248_p2 = (tmp_71_fu_241_p1 + tmp_s_fu_245_p1);

assign tmp_78_cast_fu_171_p1 = tmp_fu_163_p3;

assign tmp_80_cast_fu_280_p1 = tmp_69_fu_273_p3;

assign tmp_84_cast_fu_293_p1 = tmp_72_fu_287_p2;

assign tmp_cast_fu_284_p1 = ap_reg_pp0_iter9_j_mid2_reg_408;

assign tmp_fu_163_p3 = {{src_V_offset_offset}, {4'd0}};

assign tmp_mid2_cast_fu_225_p1 = tmp_mid2_v_reg_414;

assign tmp_mid2_v_fu_211_p3 = ((exitcond7_fu_197_p2[0:0] === 1'b1) ? i_3_fu_191_p2 : ap_phi_mux_i_phi_fu_145_p4);

assign tmp_s_fu_245_p1 = j_mid2_reg_408;

always @ (posedge ap_clk) begin
    tmp_78_cast_reg_389[3:0] <= 4'b0000;
    tmp_78_cast_reg_389[14] <= 1'b0;
    sext_reg_394[63:31] <= 33'b000000000000000000000000000000000;
end

endmodule //load_weight_2D_from_s
